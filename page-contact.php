<?php get_header(); ?>

<!-- wordpress loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-content">

  <header class="hero d-flex align-items-center">
    <!-- Set default hero header image if featured image doesn't exist -->
    <?php
      if ( has_post_thumbnail() ) {
        $hero = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
      }
      else {
        $hero = [get_template_directory_uri() . "/images/Home_Header.jpg"];
      }
    ?>

    <div class="wallpaper" style="background-image: url('<?php echo $hero['0'];?>');"></div>

    <div class="container">
      <h1><?php the_title(); ?></h1>
    </div>
  </header>

  <section class="section section-main">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <?php the_content(); ?>
          <div class="card p-2 mb-4">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2941.4602208451356!2d-92.3374567!3d42.5030269!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87e552e69c7e5a11%3A0x885ec8ba032cc2ea!2sHouse+of+Hope!5e0!3m2!1sen!2sus!4v1515388981873" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="col-md-5 offset-md-1">
          <?php echo do_shortcode("[gravityform id='1' title='false' description='false' ajax='true']"); ?>
        </div>
      </div>
    </div>

  </section>

  <section class="section section--call-to-action">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-10">
          <?php the_field('main_call_to_action'); ?>
        </div>
      </div>
    </div>

  </section>
  <section class="section--testimonial">
    <div class="testimonial-bar">
      <h2>House of Hope is tried, tested, and proven with an 93% success rate.</h2>
    </div>
  </section>
</div>

<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
