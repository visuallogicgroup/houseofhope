<?php get_header(); ?>

<!-- wordpress loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-content">

  <header class="hero d-flex align-items-center">
    <!-- Set default hero header image if featured image doesn't exist -->
    <?php
      if ( has_post_thumbnail() ) {
        $hero = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
      }
      else {
        $hero = [get_template_directory_uri() . "/images/Home_Header.jpg"];
      }
    ?>

    <div class="wallpaper" style="background-image: url('<?php echo $hero['0'];?>');"></div>

    <div class="container">
      <h1><?php the_title(); ?></h1>
    </div>
  </header>

  <section class="section section-main">
    <div class="container">
      <div class="row justify-content-md-center text-center mb-5">
        <div class="col-md-10">
          <?php the_content(); ?>
        </div>

      </div>
    </div>
  </section>

  <section class="section section--testimonial">
    <?php  echo do_shortcode("[wpv-view name='testimonial' limit='1' offset='1']"); ?>
  </section>

  <section class="section section--feature-one">
    <div class="container">
    <?php the_field('feature_one'); ?>
    </div>
  </section>

  <section class="section section--call-to-action">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-10">
          <?php the_field('main_call_to_action'); ?>
        </div>
      </div>
    </div>

  </section>
  <section class="section--testimonial">
    <div class="testimonial-bar">
      <h2>House of Hope is tried, tested, and proven with an 93% success rate.</h2>
    </div>
  </section>
</div>

<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
