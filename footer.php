

<footer class="main-footer">
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col-md-4">
        <h5>Follow us on social media.</h5>
        <a href="https://www.facebook.com/HoHCedarValley/" class="social-media-icon" target="_blank"><img src="/wp-content/themes/houseofhope/images/facebook.svg"></a>
        <a href="https://www.instagram.com/houseofhopecv/" class="social-media-icon" target="_blank"><img src="/wp-content/themes/houseofhope/images/instagram.svg"></a>
        <a href="https://twitter.com/HoHCedarValley" class="social-media-icon" target="_blank"><img src="/wp-content/themes/houseofhope/images/twitter.svg"></a>
      </div>
      <div class="col-md-4">
        <h5>Sign up for our newsletter.</h5>
        <!--<p><a href="#" class="btn btn-primary">Sign Up</a></p>-->
        <?php echo do_shortcode("[mc4wp_form id='91']"); ?>
      </div>
    </div>
  </div>
</footer>




<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

<!-- Navbar scroll add background -->
<script>
  jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 100){ // Set position from top to add class
        jQuery('.main-navbar').addClass("navbar-scrolled");
    } else {
        jQuery('.main-navbar').removeClass("navbar-scrolled");
    }
  });
</script>

<?php wp_footer(); ?>
</body>
</html>
