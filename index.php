<?php get_header(); ?>

<!-- wordpress loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-content">

  <header class="hero d-flex align-items-center">
    <div class="wallpaper"></div>
    <div class="container">
      <h1><?php the_title(); ?></h1>
    </div>
  </header>

  <section class="section section-main">
    <?php the_content(); ?>
  </section>

  <section class="section section--call-to-action">
    <div class="container">

      <?php the_field('main_call_to_action'); ?>
    </div>

  </section>
</div>

<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
