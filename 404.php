<?php get_header(); ?>

<div class="page-content">

  <header class="hero d-flex align-items-center">

    <div class="wallpaper" style="background-image: url('/wp-content/themes/houseofhope/images/Home_Header.jpg');"></div>

    <div class="container">
      <h1>Page not found</h1>
    </div>
  </header>

  <section class="section section-main">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-8">
          <h2>We're sorry, we couldn't find that page.</h2>
          <p>The <a href="/">homepage</a> is a good place to start, or you can <a href="/contact/">send us an email</a> if you need assistance. We'd be happy to help.</p>
          <a href="/" class="btn btn-primary">Visit the homepage</a>

        </div>
      </div>
    </div>

  </section>

  </section>
  <section class="section--testimonial">
    <div class="testimonial-bar">
      <h2>House of Hope is tried, tested, and proven with an 86% success rate.</h2>
    </div>
  </section>
</div>

<?php get_footer(); ?>
