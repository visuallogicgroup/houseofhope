<?php get_header(); ?>

<!-- wordpress loop for the user page content to display -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<header class="hero hero-home d-flex align-items-center">
  <!-- Set default hero header image if featured image doesn't exist -->
  <?php
    if ( has_post_thumbnail() ) {
      $hero = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
    }
    else {
      $hero = [get_template_directory_uri() . "/images/Home_Header.jpg"];
    }
  ?>

  <div class="wallpaper" style="background-image: url('<?php echo $hero['0'];?>');"></div>
  <div class="container">
    <h1><?php the_field('hero_lead'); ?></h1>
    <p><?php the_field('hero_copy'); ?><p>
    <p><a href="<?php the_field('hero_button_link'); ?>" class="btn btn-primary btn-lg"><?php the_field('hero_button_text'); ?></a></p>
  </div>
</header>

<main>
  <?php the_content(); ?>
</main>

<section class="section section--feature-one">
  <div class="container">
    <div class="row justify-content-md-center text-center mb-5">
      <div class="col-md-8">
        <?php the_field('feature_one'); ?>
      </div>
    </div>
    <?php echo do_shortcode("[wpv-view name='donation-card']"); ?>
  </div>
</section>

<section class="section section--feature-two">
  <div class="container">
    <?php the_field('feature_two'); ?>
  </div>
</section>

<section class="section section--testimonial">
  <?php  echo do_shortcode("[wpv-view name='testimonial' limit='1']"); ?>
</section>

<section class="section section--feature-three">
  <div class="container">
    <?php the_field('feature_three'); ?>
  </div>
</section>

<section class="section section--feature-four">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-md-8">
        <?php the_field('feature_four'); ?>
      </div>
    </div>
  </div>
</section>

<section class="section section--call-to-action">
  <div class="container">
    <?php the_field('main_call_to_action'); ?>
  </div>

</section>



<section class="section section--social-media text-center">
  <div class="container">
    <h2 class="mb-5">Keep up with the latest news &amp; events on Facebook.</h2>
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FHoHCedarValley%2F&tabs=timeline&width=500&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
  </div>
</section>


<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


<?php get_footer(); ?>
