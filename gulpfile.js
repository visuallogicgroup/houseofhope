const gulp = require('gulp');
const sass = require('gulp-ruby-sass');
const cleanCSS = require('gulp-clean-css');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const flexibility = require('postcss-flexibility');
const minifyCSS = require('gulp-minify-css');
const concat = require('gulp-concat');
const rename = require('gulp-rename');

gulp.task('default', ['watch'], function() {

});

gulp.task('sass', function() {
  sass('scss/style.scss')
  .on('error', sass.logError)
  .pipe(gulp.dest('./css'))
});

// Watch Files For Changes
gulp.task('watch', function() {
	gulp.watch(['./scss/**/*.scss', './**/*.js', '!./node_modules/**', '!./bower_components/**'], ['sass']);
  gulp.watch(['./css/*.css'], ['optimize-css']);
});


gulp.task('optimize-css', function() {
   return gulp.src('./css/style.css')
  	 .pipe( postcss([ require('autoprefixer'), require('postcss-flexibility') ]) )
  	 .pipe(cleanCSS({compatibility: 'ie9'}))
     .pipe(concat('style.css'))
     .pipe(rename({              //renames the concatenated CSS file
       basename : 'style',       //the base name of the renamed CSS file
       extname : '.min.css'      //the extension fo the renamed CSS file
      }))
	   .pipe(gulp.dest('./css/'))
});
