<?php

  // Register Custom Navigation Walker
  require_once('class-wp-bootstrap-navwalker.php');

  function enqueue_styles() {
    wp_enqueue_style('main-css', get_template_directory_uri() . '/css/style.min.css'  );
  }

  add_action( 'wp_enqueue_scripts', 'enqueue_styles' );


  function register_menus() {
    register_nav_menus(
      array(
        'primary' => __( 'Primary Menu')
      )
    );
  }
  add_action('after_setup_theme', 'register_menus');

  add_theme_support('post-thumbnails');



?>
